#!/usr/bin/env bash

export EASYRSA_BATCH="true"
export EASYRSA_REQ_CN="Registry Cache CA"
export SSL_DIRECTORY=/etc/docker/registry/ssl

echo "Creating EasyRSA Vars file..."

init_easy_rsa() {
    if [ ! -f "/opt/ca/pki/.ca-initialized" ]; then
        echo "Setting up EasyRSA Vars..."
        cat <<EOF >./vars
set_var EASYRSA_REQ_COUNTRY    "${EASYRSA_REQ_COUNTRY}"
set_var EASYRSA_REQ_PROVINCE   "${EASYRSA_REQ_PROVINCE}"
set_var EASYRSA_REQ_CITY       "${EASYRSA_REQ_CITY}"
set_var EASYRSA_REQ_ORG        "${EASYRSA_REQ_ORG}"
set_var EASYRSA_REQ_EMAIL      "${EASYRSA_REQ_EMAIL}"
set_var EASYRSA_REQ_OU         "${EASYRSA_REQ_OU}"
set_var EASYRSA_ALGO           "${EASYRSA_ALGO}"
set_var EASYRSA_DIGEST         "${EASYRSA_DIGEST}"
EOF

        echo "Generated EasyRSA Vars:"
        cat ./vars

        echo "Initializing PKI"
        ./easyrsa init-pki
        dd if=/dev/urandom of=/opt/ca/pki/.rnd bs=256 count=1
        touch /opt/ca/pki/.ca-initialized
    fi
}

gen_ca() {
    if [ ! -f '/opt/ca/.ca-created' ]; then
        echo "Creating CA..."
        ./easyrsa build-ca nopass
        touch /opt/ca/.ca-created
    fi
}

gen_cert() {
    if [ -f "/opt/ca/.configured" ]; then
        echo "certificate already generated"
        exit 0
    fi
    openssl req -nodes -newkey rsa:4096 -keyout /opt/ca/registry.key -out /opt/ca/registry.csr -subj \
        /C=${EASYRSA_REQ_COUNTRY}/ST=${EASYRSA_REQ_PROVINCE}/L=${EASYRSA_REQ_CITY}/O=${EASYRSA_REQ_ORG}/OU=${EASYRSA_REQ_OU}/CN=${HTTP_HOST}
    openssl req -in "/opt/ca/registry.csr" -noout -subject
    ./easyrsa import-req "/opt/ca/registry.csr" registry
    ./easyrsa sign-req server registry
}

while (("$#")); do
    case "$1" in
    --init)
        init_easy_rsa
        exit 0
        ;;

    --gen-ca)
        gen_ca
        exit 0
        ;;
    --gen-cert)
        gen_cert
        exit 0
        ;;
    *)
        echo "Invalid Argument"
        exit 1
        ;;
    esac
done

#!/bin/bash

mkdir -pv /opt/ca
mkdir -pv /etc/docker/registry/ssl

ln -s /usr/share/easy-rsa/* /opt/ca/ || true

if [ ! -f "/opt/ca/.configured" ]; then
    while true; do
        echo "waiting to finish configuration ..."
        sleep 1
    done
fi

cp -f /opt/ca/pki/issued/registry.crt /etc/docker/registry/ssl/registry.crt
cp -f /opt/ca/pki/ca.crt /etc/docker/registry/ssl/ca.crt
cp -f /opt/ca/registry.key /etc/docker/registry/ssl/registry.key

sleep 1

/entrypoint.sh /etc/docker/registry/config.yml

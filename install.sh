#!/usr/bin/env bash

dc="docker-compose --ansi never"
dcr="$dc exec"

if [ ! -f ".env" ]; then
    echo "Missing '.env' file!"
    echo "Copy '.env.template' to '.env' and then run this script again."
    exit 1
fi

if [ ! -f "docker-compose.yml" ]; then
    echo "Missing compose file!"
    echo "Copy 'docker-compose.yml.template' to 'docker-compose.yml' and then run this script again."
    exit 1
fi

source .env

if [ ! -f "registry-config.yml" ]; then
    touch registry-config.yml
fi

$dc down

echo "Initializing Registery config ..."
bash scripts/init.sh
echo "Registery config initialized."

echo "Building Docker images ..."
$dc build --force-rm registry
$dc up --no-start
echo "Docker images and services built."

echo "Starting registry container"
$dc up --no-deps -d registry

sleep 5

echo "Initializing EasyRSA ..."
$dcr -w "/opt/ca" registry /usr/local/bin/ssl.sh --init
echo "EasyRSA Initialized."
echo "Initializing CA ..."
$dcr -w "/opt/ca" registry /usr/local/bin/ssl.sh --gen-ca
echo "CA Initialized."
init_date=$(date)
$dcr registry touch /opt/ca/pki/.ca-initialized

echo "Generating Registry SSL cert ..."
$dcr -w "/opt/ca" registry bash /usr/local/bin/ssl.sh --gen-cert
echo "Registry SSL cert generated."

$dcr registry touch /opt/ca/.configured

$dcr registry cat /opt/ca/pki/ca.crt >registry-ca.crt

echo "Stopping Registry container ..."
docker-compose stop registry

echo ""
echo ""
echo "You can now start the registry with the following command:"
echo "$ docker-compose up -d"

echo ""
echo ""
echo "Add the following line to '/etc/docker/daemon.json' on any host that you"
echo "would like to have use the pull through cache."
echo ""
echo "{"
echo "  \"registry-mirrors\": [\"https://${HTTP_HOST}\"]"
echo "}"

host_ip=$(ifconfig | sed -n 's/.*inet \([0-9.]\+\)\s.*/\1/p' | grep -v ".1$")

echo ""
echo ""
echo "Add the following to '/etc/hosts' on a client host to access by DNS name"
echo "that the certificate was created for."
echo ""
echo "${host_ip} ${HTTP_HOST}"

echo ""
echo ""
echo "To avoid extra configuration around insecure registries since this cache"
echo "is using a selfsigned CA and Certificate, add the 'registry-ca.crt' file in"
echo "this directory to the system trust."
echo ""
echo "Ubuntu/Debian Example:"
echo ""
echo "$ sudo cp /path/to/ca.crt /usr/local/share/ca-certificates/"
echo "$ sudo update-ca-certificates"
echo ""
echo "Fedora/RedHat Example:"
echo ""
echo "$ sudo cp /path/to/ca.crt /etc/pki/ca-trust/source/anchors/"
echo "$ sudo update-ca-trust"
